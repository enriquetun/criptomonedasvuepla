



new Vue({
    el: '#app',
    data () {
        return {
            name: 'Bitcoin',
            symbol: 'BTC',
            img: 'https://cryptologos.cc/logos/bitcoin-btc-logo.png',
            changeperson: 10,
            pricesWithDays: [
                {day: 'Lunes', value: 123},
                {day: 'Martes', value: 203},
                {day: 'Miercoles', value: 103},
                {day: 'Jueves', value: 23},
                {day: 'Viernes', value: 253},
                {day: 'Sabado', value: 128},
                {day: 'Domingo', value: 456},
            ],
            showPrices: false,  
            value: 0,
            price: 15
        }
    },
    computed: {
        title () {
            return `${this.name} - ${this.symbol}`
        },
        convertedValue () {
            if (!this.value) {
                return 0 
            }
            return this.value /  this.price
        }
    },
    watch: {
        showPrices (newVal, oldVal) {
            console.log(newVal, oldVal)
        }
    },
    methods: {
        toggleShowPrices() {
            this.showPrices = !this.showPrices
        }
    }
})